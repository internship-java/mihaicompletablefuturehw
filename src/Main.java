import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Main {
    public static void main(String[] args) {

        int n = 4;

        CompletableFuture<String> resultatul = factorial(n)
                .thenCombine(logaritm(new Double(n)), (f, l) -> f + l)
                .thenApply(partial -> partial + 7)
                .thenApply(res -> "Result = " + res);
        futureResult(resultatul);


        CompletableFuture<String> resultLog = logaritm(rec(n))
                .thenApply(partial -> partial + 7)
                .thenApply(res -> "Result logaritm (factorial) = " + res);
        futureResult(resultLog);


    }


    private static CompletableFuture<Double> logaritm(Double d) {
        return CompletableFuture.supplyAsync(() -> Math.log10(d));
    }

    private static int recFactorial(int n) {
        if (n == 0 || n == 1) return 1;
        return n * recFactorial(n - 1);
    }

    private static double rec(int n) {
        if (n == 0 || n == 1) return 1;
        return n * recFactorial(n - 1);
    }

    private static CompletableFuture<Double> factorial(int n) {
        return CompletableFuture.supplyAsync(() -> new Double(recFactorial(n)));
    }

    private static void futureResult(CompletableFuture<String> text) {

        try {

            System.out.println(text.get());

        } catch (InterruptedException e) {

            e.printStackTrace();

        } catch (ExecutionException e) {

            e.printStackTrace();

        }

    }
}